import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FindAlarmsRepository } from '@application/ports';
import { MaterializedAlarmView } from './../schemas/materialized-alarm-view.schema';
import { AlarmReadModel } from '@domain/read-models';

@Injectable()
export class OrmFindAlarmsRepository implements FindAlarmsRepository {
  constructor(
    @InjectModel(MaterializedAlarmView.name)
    private readonly AlarmReadModel: Model<MaterializedAlarmView>,
  ) {}

  async findAll(): Promise<AlarmReadModel[]> {
    return await this.AlarmReadModel.find();
  }
}
