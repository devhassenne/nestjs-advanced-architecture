import { AlarmReadModel } from '@domain/read-models';

export abstract class FindAlarmsRepository {
  abstract findAll(): Promise<AlarmReadModel[]>;
}
