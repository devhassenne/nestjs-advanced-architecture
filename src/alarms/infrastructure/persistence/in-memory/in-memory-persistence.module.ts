import { Module } from '@nestjs/common';
import { CreateAlarmRepository } from '@application/ports';
import { InMemoryAlarmRepository } from './repositories/alarm.repository';
import {
  FindAlarmsRepository,
  UpsertMaterializedAlarmRepository,
} from '@application/ports';

@Module({
  imports: [],
  providers: [
    InMemoryAlarmRepository,
    {
      provide: CreateAlarmRepository,
      useExisting: InMemoryAlarmRepository, // 👈
    },
    {
      provide: FindAlarmsRepository,
      useExisting: InMemoryAlarmRepository, // 👈
    },
    {
      provide: UpsertMaterializedAlarmRepository,
      useExisting: InMemoryAlarmRepository, // 👈
    },
  ],
  exports: [
    // 👈
    CreateAlarmRepository,
    FindAlarmsRepository,
    UpsertMaterializedAlarmRepository,
  ],
})
export class InMemoryAlarmPersistenceModule {}
