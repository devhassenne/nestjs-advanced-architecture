import { Alarm } from '@domain/alarm';

export class AlarmCreatedEvent {
  constructor(public readonly alarm: Alarm) {}
}
