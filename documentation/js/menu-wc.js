'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">advanced-architecture documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AlarmsInfrastructureModule.html" data-type="entity-link" >AlarmsInfrastructureModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AlarmsModule.html" data-type="entity-link" >AlarmsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-AlarmsModule-0876139118ef7a88a39b346d2188e705a34531bdab630a178291e8c42802a113bef2e6ffe3bb9c5ff8c2fa100290135227dda66faa15894c048e61e1311e5ca2"' : 'data-bs-target="#xs-controllers-links-module-AlarmsModule-0876139118ef7a88a39b346d2188e705a34531bdab630a178291e8c42802a113bef2e6ffe3bb9c5ff8c2fa100290135227dda66faa15894c048e61e1311e5ca2"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AlarmsModule-0876139118ef7a88a39b346d2188e705a34531bdab630a178291e8c42802a113bef2e6ffe3bb9c5ff8c2fa100290135227dda66faa15894c048e61e1311e5ca2"' :
                                            'id="xs-controllers-links-module-AlarmsModule-0876139118ef7a88a39b346d2188e705a34531bdab630a178291e8c42802a113bef2e6ffe3bb9c5ff8c2fa100290135227dda66faa15894c048e61e1311e5ca2"' }>
                                            <li class="link">
                                                <a href="controllers/AlarmsController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AlarmsController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-AlarmsModule-0876139118ef7a88a39b346d2188e705a34531bdab630a178291e8c42802a113bef2e6ffe3bb9c5ff8c2fa100290135227dda66faa15894c048e61e1311e5ca2"' : 'data-bs-target="#xs-injectables-links-module-AlarmsModule-0876139118ef7a88a39b346d2188e705a34531bdab630a178291e8c42802a113bef2e6ffe3bb9c5ff8c2fa100290135227dda66faa15894c048e61e1311e5ca2"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AlarmsModule-0876139118ef7a88a39b346d2188e705a34531bdab630a178291e8c42802a113bef2e6ffe3bb9c5ff8c2fa100290135227dda66faa15894c048e61e1311e5ca2"' :
                                        'id="xs-injectables-links-module-AlarmsModule-0876139118ef7a88a39b346d2188e705a34531bdab630a178291e8c42802a113bef2e6ffe3bb9c5ff8c2fa100290135227dda66faa15894c048e61e1311e5ca2"' }>
                                        <li class="link">
                                            <a href="injectables/AlarmFactory.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AlarmFactory</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/AlarmsService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AlarmsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-AppModule-d63ab1fbbd125e434b4de4a25c88ce09098a9f5321527df871173d68ed89bdb436f8cf8fcec043a9acdbe95575383e1798b4b2d69863fbb2eeb34c5e5440a1bd"' : 'data-bs-target="#xs-controllers-links-module-AppModule-d63ab1fbbd125e434b4de4a25c88ce09098a9f5321527df871173d68ed89bdb436f8cf8fcec043a9acdbe95575383e1798b4b2d69863fbb2eeb34c5e5440a1bd"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-d63ab1fbbd125e434b4de4a25c88ce09098a9f5321527df871173d68ed89bdb436f8cf8fcec043a9acdbe95575383e1798b4b2d69863fbb2eeb34c5e5440a1bd"' :
                                            'id="xs-controllers-links-module-AppModule-d63ab1fbbd125e434b4de4a25c88ce09098a9f5321527df871173d68ed89bdb436f8cf8fcec043a9acdbe95575383e1798b4b2d69863fbb2eeb34c5e5440a1bd"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-AppModule-d63ab1fbbd125e434b4de4a25c88ce09098a9f5321527df871173d68ed89bdb436f8cf8fcec043a9acdbe95575383e1798b4b2d69863fbb2eeb34c5e5440a1bd"' : 'data-bs-target="#xs-injectables-links-module-AppModule-d63ab1fbbd125e434b4de4a25c88ce09098a9f5321527df871173d68ed89bdb436f8cf8fcec043a9acdbe95575383e1798b4b2d69863fbb2eeb34c5e5440a1bd"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-d63ab1fbbd125e434b4de4a25c88ce09098a9f5321527df871173d68ed89bdb436f8cf8fcec043a9acdbe95575383e1798b4b2d69863fbb2eeb34c5e5440a1bd"' :
                                        'id="xs-injectables-links-module-AppModule-d63ab1fbbd125e434b4de4a25c88ce09098a9f5321527df871173d68ed89bdb436f8cf8fcec043a9acdbe95575383e1798b4b2d69863fbb2eeb34c5e5440a1bd"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/InMemoryAlarmPersistenceModule.html" data-type="entity-link" >InMemoryAlarmPersistenceModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/OrmAlarmPersistenceModule.html" data-type="entity-link" >OrmAlarmPersistenceModule</a>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#controllers-links"' :
                                'data-bs-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AlarmsController.html" data-type="entity-link" >AlarmsController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#entities-links"' :
                                'data-bs-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/AlarmEntity.html" data-type="entity-link" >AlarmEntity</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Alarm.html" data-type="entity-link" >Alarm</a>
                            </li>
                            <li class="link">
                                <a href="classes/AlarmCreatedEvent.html" data-type="entity-link" >AlarmCreatedEvent</a>
                            </li>
                            <li class="link">
                                <a href="classes/AlarmCreatedEventHandler.html" data-type="entity-link" >AlarmCreatedEventHandler</a>
                            </li>
                            <li class="link">
                                <a href="classes/AlarmEntity.html" data-type="entity-link" >AlarmEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/AlarmMapper.html" data-type="entity-link" >AlarmMapper</a>
                            </li>
                            <li class="link">
                                <a href="classes/AlarmMapper-1.html" data-type="entity-link" >AlarmMapper</a>
                            </li>
                            <li class="link">
                                <a href="classes/AlarmRepository.html" data-type="entity-link" >AlarmRepository</a>
                            </li>
                            <li class="link">
                                <a href="classes/AlarmSeverity.html" data-type="entity-link" >AlarmSeverity</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateAlarmCommand.html" data-type="entity-link" >CreateAlarmCommand</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateAlarmCommandHandler.html" data-type="entity-link" >CreateAlarmCommandHandler</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateAlarmDto.html" data-type="entity-link" >CreateAlarmDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetAlarmsQuery.html" data-type="entity-link" >GetAlarmsQuery</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetAlarmsQueryHandler.html" data-type="entity-link" >GetAlarmsQueryHandler</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateAlarmDto.html" data-type="entity-link" >UpdateAlarmDto</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#injectables-links"' :
                                'data-bs-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AlarmFactory.html" data-type="entity-link" >AlarmFactory</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AlarmsService.html" data-type="entity-link" >AlarmsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/InMemoryAlarmRepository.html" data-type="entity-link" >InMemoryAlarmRepository</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OrmAlarmRepository.html" data-type="entity-link" >OrmAlarmRepository</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#interfaces-links"' :
                            'data-bs-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/ApplicationBootstrapOptions.html" data-type="entity-link" >ApplicationBootstrapOptions</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});