import { DynamicModule, Module } from '@nestjs/common';
import { AlarmsModule } from '@application/alarms.module';
import { AlarmsInfrastructureModule } from '@infrastructure/alarms-infrastructure.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ApplicationBootstrapOptions } from '@common/interfaces';
import { CoreModule } from '@core/core.module';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [CoreModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static register(options: ApplicationBootstrapOptions): DynamicModule {
    // 👈 new method
    return {
      module: AppModule,
      imports: [
        CqrsModule.forRoot(),
        CoreModule.forRoot(options),
        AlarmsModule.withInfrastucture(
          AlarmsInfrastructureModule.use(options.driver),
        ),
      ],
    };
  }
}
