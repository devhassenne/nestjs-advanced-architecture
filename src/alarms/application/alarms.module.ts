import { DynamicModule, Module, Type } from '@nestjs/common';
import { AlarmFactory } from '@domain/factories';
import { AlarmsController } from '@presenters/http';
import { AlarmsService } from '@application/alarms.service';
import { CreateAlarmCommandHandler } from '@application/commands';
import { GetAlarmsQueryHandler } from '@application/queries';
import { AlarmCreatedEventHandler } from '@application/event-handlers';

@Module({
  controllers: [AlarmsController],
  providers: [
    AlarmsService,
    AlarmFactory,
    CreateAlarmCommandHandler,
    GetAlarmsQueryHandler,
    AlarmCreatedEventHandler,
  ],
})
export class AlarmsModule {
  static withInfrastucture(infrastructureModule: Type | DynamicModule) {
    // 👈 new static method
    return {
      module: AlarmsModule,
      imports: [infrastructureModule],
    };
  }
}
