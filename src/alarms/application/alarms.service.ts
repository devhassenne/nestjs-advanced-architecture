import { Injectable } from '@nestjs/common';
// import { AlarmFactory } from '@domain/factories/alarm.factory';
import { CreateAlarmCommand } from '@application/commands';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { GetAlarmsQuery } from './queries';

@Injectable()
export class AlarmsService {
  constructor(
    private readonly commandBus: CommandBus, // 👈
    private readonly queryBus: QueryBus, // 👈
  ) {}

  create(createAlarmCommand: CreateAlarmCommand) {
    return this.commandBus.execute(createAlarmCommand); // 👈
  }

  findAll() {
    return this.queryBus.execute(new GetAlarmsQuery()); // 👈
  }
}
